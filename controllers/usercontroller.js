/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
 
var exports = module.exports = {}
//var q = require('q');
var Bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

//var sequelize = require('../models');
var model = require('../models/index');
var salt = Bcrypt.genSaltSync(10);
//var cert = fs.readFileSync('../config/private.key');  // get private key


//var User  = require('../models').User;

exports.signup =  function(req,res){
		//console.log(userModel.toString())	
		return model.user.findAll({
                where: {
                    email_id: req.body.email_id
                }
            })
		.then(result => {
			if (result.length==0){
				
				var password = req.body.password 

				var encryptedPassword = Bcrypt.hashSync(password, salt);
				//var orgPassword = Bcrypt.compareSync(password, encryptedPassword);

				req.body.password = encryptedPassword
				return model.user.create(req.body)
				.then(response=>{
					return {
						"status":true,
						"message" : "Created Successfully!!",
						"body":req.body
					}
				})
			}
			else{
				var err =  new Error('User Already Exists!!')
				err.status_code = 200;
				throw err;	
			}
		})/*.catch(err =>{
			console.log(err.status_code)
			var error =  new Error(err.message)
				error.status_code = err.status_code || 400;
				error.stack = err;
				throw error;
		})*/
		
	
}

exports.signin = function(req,res){
	console.log(req.body)
	/*var salt = Bcrypt.genSaltSync(10);*/
	var password = req.body.password;
	//var orgPassword = Bcrypt.compareSync(password, encryptedPassword);
	return model.user.findOne({
                where: {
                    email_id: req.body.email_id,
                    //password: encryptedPassword,
                    status :'active'
                }
            }).then(result=>{
            	//console.log(result)
            	////check is user exists
            	if(!result)
            	{
            		var err = new Error("Please provide valid credentials");
            		err.status_code=403;
            		throw err;
            	}
            	var userData = JSON.parse(JSON.stringify(result));
            	//console.log(result);
            	///check for valid password
            	var hash = Bcrypt.compareSync(password, userData.password); 
            	if(!hash)
            	{
            		var err = new Error("Please provide valid credentials");
            		err.status_code=403;
            		throw err;
            	}
            	return res.json(
            		{
            			token: jwt.sign(
            					{ 
            					email_id: userData.email_id,
            				 	first_name: userData.first_name,
            				 	last_name: userData.last_name,
            				 	_id: userData.id
            				 	}, 
            				 	'thisissparta',
            				 	{ expiresIn: '1h'})
            		});

            })
}

exports.loginRequired = function(req, res, next) {
  if (req.user) {
    next();
  } else {
  	/*var err = new Error("Unauthorized User");
            		err.status_code=401;
            		throw err;*/
    return res.status(401).json({ 
    	message: 'Unauthorized user!' ,
    	status : false
    	});
  }
};



