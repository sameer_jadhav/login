/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
var exports = module.exports = {}
var jwt = require('jsonwebtoken');
//var sequelize = require('../models');
var model = require('../models/index');

exports.listCustomer =  function(req,res){
		//console.log(userModel.toString())	
	return model.user.findAll()
		.then(response => {
			///console.log(response)
			return {
						"status":true,
						"message" : "Authorized User",
						"body":""
					}
		});
		
	
}
