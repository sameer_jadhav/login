module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('user', {
 
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
 
        first_name: {
            type: DataTypes.STRING,
            notEmpty: true,
            allowNull: false
        },
 
        last_name: {
            type: DataTypes.STRING,
            notEmpty: true,
            allowNull: false
        },
 
        mobile_no: {
            type: DataTypes.STRING,
            allowNull: false
        },      
        email_id: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            allowNull: false
        },
 
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
 
        last_login: {
            type: DataTypes.DATE
        },
 
        status: {
            type: DataTypes.ENUM('active', 'inactive'),
            defaultValue: 'active'
        }
 
 
    });
 
    return User;
 
}