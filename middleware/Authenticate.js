let authenticate = function () {
	return function(req, res, next) {
		
		
		  if (req.headers && req.headers.authorization ) {
		    jsonwebtoken.verify(req.headers.authorization, 'thisissparta', function(err, decode) {
		      if (err) req.user = undefined;
		      req.user = decode;
		      next();
		    });
		  } else {
		    req.user = undefined;
		    /*return res.status(401).json({ 
		    	message: 'Unauthorized user!' ,
		    	status : false
		    	});*/
		    next();
		  }
		}
}

module.exports = authenticate;
/*export {
	authenticate
}*/
