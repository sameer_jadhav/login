var express = require('express');
var router = express.Router();
var users = require('./Users');
var customers = require('./Customers');
var IndexRoute = require('./IndexRoute');
var winston = require('winston');
var mung = require('express-mung');

//console.log(customers)
var authenticate = require('./../middleware');
jsonwebtoken = require("jsonwebtoken");

//console.log(authenticate)

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/
/*const Indexroute = function(req,res,next){
	res.render('index', { title: 'Express' });
}*/

module.exports = function (app) {
	//console.log(app)
		router.use('/', IndexRoute);
		router.use('/users',users);
		//router.use('/',Indexroute);
		app.use(mung.json(
		    function transform(body, req, res) {
		    	
		        winston.log('info', {  reqDetails: "HTTP "+req.method+" "+req.url , responseBody:JSON.stringify(body)});
		        return body;
		    }
		));
		
		app.use(authenticate());
		
		router.use('/customers',customers);
		
		return router;
};
