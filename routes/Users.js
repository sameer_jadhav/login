var express = require('express');
var router = express.Router();
var userController = require('../controllers/usercontroller.js');
var winston = require('winston');
//console.log(winston)

/* GET users listing. */
router.get('/signup', function(req, res, next) {
  res.status(500);
  res.send({
    status: false,
    message: 'Method not allowed'
  });
});
router.put('/signup', function(req, res, next) {
  res.status(500);
  res.send({
    status: false,
    message: 'Method not allowed'
  });
});
router.delete('/signup', function(req,res,next) {
  res.status(500);
  res.send({
    status: false,
    message: 'Method not allowed'
  });
});

router.post('/signup', function(req, res, next) {
  userController.signup(req,res)
  	.then((response)=>{
      //console.log(response)
        //winston.log('info', response)
  		res.send(response);
  	})
  	.catch(err => { 
       //winston.log('error', err)
  		res.status(err.status_code || 400).json({
            status:false,
            message: err.message,
            stack : err.stack
        })
  	});
});

router.get('/signin', function(req, res, next) {
  res.status(500);
  res.send({
    status: false,
    message: 'Method not allowed'
  });
});
router.put('/signin', function(req, res, next) {
  //res.status(500).send('Method not allowed');
    res.status(500);
    res.send({
      status: false,
      message: 'Method not allowed'
    });
});
router.delete('/signin', function(req,res,next) {
  //res.status(500).send('Method not allowed');
    res.status(500);
    res.send({
      status: false,
      message: 'Method not allowed'
    });
});

router.post('/signin', function(req, res, next) {
  userController.signin(req,res)
  	.then((response)=>{
     
  		res.send(response);
      

  	})
  	.catch(err => { 
  		res.status(err.status_code || 400).json({
            status:false,
            message: err.message,
            stack : err.stack
        })
  	});
});



module.exports = router;
