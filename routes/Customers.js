var express = require('express');
var router = express.Router();
var winston = require('winston');
var userController = require('../controllers/usercontroller.js');
var customerController = require('../controllers/customercontroller.js');

router.get('/', function(req, res, next) {
  //res.status(500).send('Method not allowed');
  userController.loginRequired(req,res,customerController.listCustomer);
  customerController.listCustomer(req,res)
  .then(response=>{
  		
  		res.send(response);
  })
  .catch(err => { 
  		res.status(err.status_code || 400).json({
            status:false,
            message: err.message,
            stack : err.stack
        })
  	});
});


module.exports = router;